'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    babel = require('gulp-babel'),
    browserSync = require('browser-sync').create(),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    minifyCss = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    stripCssComments = require('gulp-strip-css-comments'),
    rimraf = require('rimraf'),
    pug = require('gulp-pug'),
    webpack = require('webpack-stream'),
    plumber = require('gulp-plumber'),
    plumberNotifier = require('gulp-plumber-notifier'),
    htmlbeautify = require('gulp-html-beautify'),
    rigger = require('gulp-rigger');

// новые переменные

var
    source = 'dev/',
    dest = 'build/';

var bootstrapSass = { in: './node_modules/bootstrap-sass/'
};

var fonts = { in: [source + 'fonts/*.*', bootstrapSass.in + 'assets/fonts/**/*'],
    out: dest + 'fonts/'
};


var scss = { in: source + 'scss/main.scss',
    out: dest + 'style/',
    watch: source + 'scss/**/*',
    sassOpts: {
        outputStyle: 'compact',
        precison: 3,
        errLogToConsole: true,
        includePaths: [bootstrapSass.in + 'assets/stylesheets']
    }
};

// старые переменные

var path = {
    build: {
        html: 'build/',
        pug: 'build/',
        js: 'build/js/',
        css: 'build/style/',
        gfx: 'build/style/gfx',
        img: 'build/images',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'dev/html/*.html',
        pug: 'dev/pug/*.pug',
        js: [
            'dev/js/script.js',
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/popper.js/dist/popper.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js'
        ],
        style: 'dev/scss/main.scss',
        gfx: 'dev/style/gfx/**/*.*',
        img: 'dev/images/**/*.*',
        fonts: 'dev/fonts/**/*.*'
    },
    watch: {
        html: 'dev/html/*.html',
        pug: 'dev/pug/**/*.pug',
        js: 'dev/js/**/*.js',
        style: 'dev/scss/**/*.scss',
        gfx: 'dev/style/gfx/**/*.*',
        img: 'dev/images/**/*.*',
        fonts: 'dev/fonts/**/*.*'
    },
    clean: './build'
};

// сборка HTML

gulp.task('html:build', function() {
    gulp.src(path.src.html)
        // .pipe(pug())
        .pipe(gulp.dest(path.build.html));
});

gulp.task('pug:build', function() {
    gulp.src(path.src.pug)
        .pipe(plumber())
        .pipe(plumberNotifier())
        .pipe(pug())
        .pipe(htmlbeautify({
            indentSize: 2
        }))
        .pipe(gulp.dest(path.build.pug));
});

// сборка JavaScript

gulp.task('js:build', function() {
    gulp.src(path.src.js)
        //.pipe(sourcemaps.init())
        .pipe(rigger())
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(uglify())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream());
});

// сборка SCSS

gulp.task('sass', function() {
    return gulp.src(scss.in)
        .pipe(plumber())
        .pipe(plumberNotifier())
        .pipe(sass(scss.sassOpts))
        .pipe(minifyCss())
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(scss.out))
        .pipe(browserSync.stream());
});

// копирование графики

gulp.task('image:build', function() {
    gulp.src(path.src.gfx)
        .pipe(gulp.dest(path.build.gfx));
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
});

// копирование шрифтов

gulp.task('fonts', function() {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

// watch

gulp.task('serve', [
    'sass',
    'fonts',
    'image:build',
    'js:build',
    'html:build',
    'pug:build'
], function() {

    browserSync.init({
        server: "./build"
    });

    gulp.watch(scss.watch, ['sass']);
    gulp.watch(path.watch.image, ['image:build']);
    gulp.watch(path.watch.js, ['js:build']);
    gulp.watch(path.watch.pug, ['pug:build']).on('change', browserSync.reload);
    gulp.watch(path.watch.html, ['html:build']).on('change', browserSync.reload);
});

gulp.task('default', ['serve']);
