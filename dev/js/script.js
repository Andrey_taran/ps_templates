$(document).ready(function() {

    $(".header__authorization .button").click(function() {
        $(".overlay_login").addClass("overlay_active");
        $(".page").addClass("page_blur");
    });
    $(".b-modal .b-modal__close").click(function() {
        $(".overlay").removeClass("overlay_active");
        $(".page").removeClass("page_blur");
    });

    $(".business_request").click(function() {
        $(".overlay_business").addClass("overlay_active");
        $(".page").addClass("page_blur");
    });

    function showmenu() {
        $(".sidebar").addClass("sidebar_active");
        $("body").css("overflow", "hidden");
    };

    function hidemenu() {
        $(".sidebar").removeClass("sidebar_active");
        $("body").css("overflow", "visible");
    };

    $(function() {
        $(".menu-toggle").click(function() {
            showmenu();
        });
        $(".sidebar__close").click(function() {
            hidemenu();
        });
    });

    // file-select

    // var inputs = document.querySelectorAll('.file-select__input');
    // Array.prototype.forEach.call(inputs, function(input) {
    //     var label = input.nextElementSibling,
    //         labelVal = label.innerHTML;
    // 
    //     input.addEventListener('change', function(e) {
    //         var fileName = '';
    //         if (this.files && this.files.length > 1)
    //             fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
    //         else
    //             fileName = e.target.value.split('\\').pop();
    // 
    //         if (fileName) {
    //             label.querySelector('span').innerHTML = fileName;
    //             $('.file-select__remove').css("display", "inline-block");
    //         } else
    //             label.innerHTML = labelVal;
    //     });
    // 
    //     $('.file-select__remove').click(function() {
    //         $(this).css("display", "none");
    //         label.innerHTML = labelVal;
    //         $(this).siblings("input").val("");
    //     });
    // });
    // 
    // 
    // $(document).ready(function() {
    //     $('input[type = "file"]').change(function(e) {
    //         var fileName = e.target.files[0].name;
    //         $('.file-select__label span').innerHTML = "File" + fileName + "is selected.";
    //     });
    // });

});
