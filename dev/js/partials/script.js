$(document).ready(function() {

    $(".header__authorization .button").click(function(){
        $(".overlay_login").addClass("overlay_active");
        $(".page").addClass("page_blur");
    });
    $(".modal .modal__close").click(function(){
        $(".overlay").removeClass("overlay_active");
        $(".page").removeClass("page_blur");
    });
    
    $(".business_request").click(function(){
        $(".overlay_business").addClass("overlay_active");
        $(".page").addClass("page_blur");
    });

    function showmenu() {
        $(".sidebar").addClass("sidebar_active");
        $("body").css("overflow", "hidden");
    };

    function hidemenu() {
        $(".sidebar").removeClass("sidebar_active");
        $("body").css("overflow", "visible");
    };

    $(function() {
        $(".menu-toggle").click(function() {
            showmenu();
        });
        $(".sidebar__close").click(function() {
            hidemenu();
        });
    });
});
