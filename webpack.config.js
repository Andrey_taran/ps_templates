const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: './dev/index.js',
    output: {
        filename: 'script.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {

    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            Popper: ['popper.js', 'default']
        })
    ],
    externals: {
        $: "jquery",
        jQuery: "jquery"
    }
};
